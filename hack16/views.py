"""
Views file
"""

from django.shortcuts import render


def index(request):
    """
    Main page

    :param request:
    :return:
    """
    return render(request, 'index.html')